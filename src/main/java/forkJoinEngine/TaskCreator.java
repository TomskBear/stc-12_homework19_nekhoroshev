package forkJoinEngine;

import Model.Sentence;
import interfaces.ITask;

import java.util.concurrent.ConcurrentLinkedQueue;

public class TaskCreator {
    public ITask createTask(String[] sources, int start, int end, ConcurrentLinkedQueue<Sentence> sentenceForSearchQueue) {
        return new FillParsedSentenceQueueTask(sources, start, end, sentenceForSearchQueue);
    }
}
